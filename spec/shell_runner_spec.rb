require 'rspec'
require_relative '../tanuki/tanuki.rb'

describe "Shell Secret Masking" do
    tanuki = Tanuki.new

    it "masks the variable and doesn't display it's value" do
        trace = tanuki.get_job_trace('shell-hello-world')
        expect(trace).to include('[MASKED]')
        expect(trace).not_to include('BRUCE_WAYNE')
    end
end