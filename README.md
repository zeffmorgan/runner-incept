
# What is going on here?

Welcome to a glorious abuse of the `needs` keyword, docker-in-docker, and probably a few other features of GitLab CI.

Everything is happening in [pipelines](https://gitlab.com/erushton/runner-incept/-/pipelines). A quick look shows several stages. The first is where things get weird - the `Runners` stage has separate jobs for different runner configs. For each job, the script downloads the lastest build of `master`, and then executes and registers the runner to this project. Each registered runner is isolated with specific tags. Downstream jobs are then tagged to execute their jobs on those upstream registered runners - thus exercising the runner functionality as it appears on the master branch. "Runners" (aka jobs in the `Runners` stage) are killed after a timeout so that their job in the pipeline appears as a success. If they failed to launch then the downstream jobs would never pass so this shouldn't be possible to introduce any false-positives. After the "Runners" timeout expires the individual runners are then unregistered from the project.

Currently the `.gitlab-ci.yml` is written to make the concepts that are happening clearer instead of trying to de-duplicate things. There's obviously a ton of opportunity to remove duplication of job definitions etc.

